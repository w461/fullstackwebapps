export function WorkerName(name) {
  let data = ["Wahyunius", "ATM", "Wahyutrade", "Wahyugadai", "Benih"];
  if (name.includes("worker")) {
    let buf = name.split("worker")[1];
    if (Number(buf) > 5) {
      return name;
    }
    return data[Number(buf) - 1];
  } else {
    return name;
  }
}
