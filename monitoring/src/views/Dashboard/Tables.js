import React, { useEffect, useState } from "react";
// Chakra imports
import {
  Button,
  Center,
  Icon,
  Flex,
  Table,
  Tbody,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
  Spinner,
} from "@chakra-ui/react";
// Custom components
import { WiCloudRefresh } from "react-icons/wi";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import TablesTableRow from "components/Tables/TablesTableRow";
// import { tablesTableData } from "variables/general";
import { Backendurl } from "config";
import Axios from "axios";

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function FormatDate() {
  let data = new Date()
    .toLocaleString("id-ID")
    .replace(".", ":")
    .replace(".", ":");
  return data;
}

function Tables() {
  const textColor = useColorModeValue("gray.700", "white");
  const [app, setapp] = useState([]);
  const [triger, settriger] = useState(false);
  const [isLoading, setisLoading] = useState(true);

  useEffect(() => {
    if (localStorage.getItem("token") === null) {
      window.location.replace("/#/auth/signin");
    }
    getApps();
  }, [triger]);

  const getApps = () => {
    sleep(5000);
    Axios.get(`${Backendurl}/api/apps/find`)
      .then((res) => {
        let aplikasi = res.data;
        aplikasi.map((item) => {
          item.date = FormatDate();
          item.domain = item.host;
        });
        setapp(aplikasi);
        setisLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    setInterval(() => {
      // setisLoading(true);
      getApps();
    }, 60 * 1000);
  }, []);

  return (
    <Flex direction="column" pt={{ base: "120px", md: "75px" }}>
      <Card overflowX={{ sm: "scroll", xl: "hidden" }}>
        <CardHeader p="6px 0px 22px 0px">
          <Button
            bg="gray.200"
            _hover={{ bg: "gray.200" }}
            style={{ padding: ".2rem" }}
            onClick={() => {
              settriger(!triger);
              setisLoading(true);
            }}
          >
            <Icon w={8} h={8} as={WiCloudRefresh} />
          </Button>
          <Text
            fontSize="xl"
            color={textColor}
            fontWeight="bold"
            mt={".3rem"}
            ml={"1rem"}
          >
            Applications
          </Text>
        </CardHeader>

        {isLoading ? (
          <Center h="100px" color="white">
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="blue.500"
              size="md"
            />
            <Text
              fontSize="xl"
              color={textColor}
              fontWeight="bold"
              mt={".3rem"}
              ml={"1rem"}
              style={{ textAlign: "center" }}
            >
              Fetching Data...
            </Text>
          </Center>
        ) : (
          <CardBody>
            <Table variant="simple" color={textColor}>
              <Thead>
                <Tr my=".8rem" pl="0px" color="gray.400">
                  <Th pl="0px" color="gray.400">
                    Application Name
                  </Th>
                  <Th textAlign="center" color="gray.400">
                    Host IP
                  </Th>
                  <Th textAlign="center" color="gray.400">
                    Status
                  </Th>
                  <Th textAlign="center" color="gray.400">
                    Last Checked
                  </Th>
                  <Th textAlign="center" color="gray.400">
                    Current Job
                  </Th>
                  <Th></Th>
                </Tr>
              </Thead>
              <Tbody>
                {app.map((row) => {
                  return (
                    <TablesTableRow
                      name={row.name}
                      logo={row.logo}
                      subdomain={row.port}
                      domain={row.domain}
                      status={row.status}
                      date={row.date}
                      triger={triger}
                      settriger={settriger}
                    />
                  );
                })}
              </Tbody>
            </Table>
          </CardBody>
        )}
      </Card>
    </Flex>
  );
}

export default Tables;
