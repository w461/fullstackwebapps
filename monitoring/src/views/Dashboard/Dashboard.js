// Chakra imports
import {
  Box,
  Flex,
  Grid,
  Text,
  Progress,
  SimpleGrid,
  Spacer,
  Stat,
  StatHelpText,
  StatLabel,
  StatNumber,
  useColorModeValue,
} from "@chakra-ui/react";
// Custom components
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import BarChart from "components/Charts/BarChart";
import LineChart from "components/Charts/LineChart";
import IconBox from "components/Icons/IconBox";
// Custom icons
import {
  DocumentIcon,
  GlobeIcon,
  ClockIcon,
  RocketIcon,
} from "components/Icons/Icons.js";
import React, { useEffect, useState } from "react";
import Axios from "axios";
import { Backendurl } from "config";
import { WorkerName } from "helper/worketToName";

export default function Dashboard() {
  // Chakra Color Mode
  const iconTeal = useColorModeValue("teal.300", "teal.300");
  const iconBoxInside = useColorModeValue("white", "white");
  const textColor = useColorModeValue("gray.700", "white");
  const [log, setlog] = useState({
    dataLog: 0,
    selisih: 0,
  });
  const [app, setapp] = useState({
    run: "Please Wait",
    stop: "Please Wait",
  });

  const [data, setdata] = useState({
    data: [],
    logcount: [],
    total: 0,
  });

  useEffect(() => {
    if (localStorage.getItem("token") === null) {
      window.location.replace("/#/auth/signin");
    }
    Axios({
      method: "get",
      url: Backendurl + "/api/logs/find/total",
      headers: {},
      data: {},
    })
      .then((res) => {
        setlog(res.data);
      })
      .catch((err) => {});

    Axios({
      method: "get",
      url: Backendurl + "/api/apps/check",
      headers: {},
      data: {},
    })
      .then((res) => {
        setapp(res.data);
      })
      .catch((err) => {});

    Axios({
      method: "GET",
      url: Backendurl + "/api/logs/find",
      headers: {},
      data: {},
    })
      .then((res) => {
        let nameApps = [];
        let logApps = [];
        res.data.map((item) => {
          nameApps.push(item.name);
          logApps.push(item.count);
        });
        setdata({
          data: nameApps,
          logcount: logApps,
          total: logApps.reduce((partial_sum, a) => partial_sum + a, 0),
        });
      })
      .catch((err) => {});
  }, []);

  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  return (
    <Flex flexDirection="column" pt={{ base: "120px", md: "75px" }}>
      <SimpleGrid columns={{ sm: 1, md: 2, xl: 4 }} spacing="24px">
        <Card minH="83px">
          <CardBody>
            <Flex flexDirection="row" align="center" justify="center" w="100%">
              <Stat me="auto">
                <StatLabel
                  fontSize="sm"
                  color="gray.400"
                  fontWeight="bold"
                  pb=".1rem"
                >
                  Today's Log
                </StatLabel>
                <Flex>
                  <StatNumber fontSize="lg" color={textColor}>
                    {log.dataLog}
                  </StatNumber>
                  <StatHelpText
                    alignSelf="flex-end"
                    justifySelf="flex-end"
                    m="0px"
                    color="green.400"
                    fontWeight="bold"
                    ps="3px"
                    fontSize="md"
                  >
                    +{log.selisih}
                  </StatHelpText>
                </Flex>
              </Stat>
              <IconBox as="box" h={"45px"} w={"45px"} bg={iconTeal}>
                <DocumentIcon h={"24px"} w={"24px"} color={iconBoxInside} />
              </IconBox>
            </Flex>
          </CardBody>
        </Card>
        <Card minH="83px">
          <CardBody>
            <Flex flexDirection="row" align="center" justify="center" w="100%">
              <Stat me="auto">
                <StatLabel
                  fontSize="sm"
                  color="gray.400"
                  fontWeight="bold"
                  pb=".1rem"
                >
                  Total Apps
                </StatLabel>
                <Flex>
                  {app.run === "Please Wait" && app.stop === "Please Wait" ? (
                    <StatNumber fontSize="sm" color={textColor}>
                      Please Wait
                    </StatNumber>
                  ) : (
                    <StatNumber fontSize="lg" color={textColor}>
                      {app.run + app.stop}
                    </StatNumber>
                  )}
                </Flex>
              </Stat>
              <IconBox as="box" h={"45px"} w={"45px"} bg={iconTeal}>
                <GlobeIcon h={"24px"} w={"24px"} color={iconBoxInside} />
              </IconBox>
            </Flex>
          </CardBody>
        </Card>
        <Card minH="83px">
          <CardBody>
            <Flex flexDirection="row" align="center" justify="center" w="100%">
              <Stat>
                <StatLabel
                  fontSize="sm"
                  color="gray.400"
                  fontWeight="bold"
                  pb=".1rem"
                >
                  Run Apps
                </StatLabel>
                <Flex>
                  {app.run === "Please Wait" ? (
                    <StatNumber fontSize="sm" color={textColor}>
                      Please Wait
                    </StatNumber>
                  ) : (
                    <StatNumber fontSize="lg" color={textColor}>
                      {app.run}
                    </StatNumber>
                  )}
                </Flex>
              </Stat>
              <Spacer />
              <IconBox as="box" h={"45px"} w={"45px"} bg={iconTeal}>
                <RocketIcon h={"24px"} w={"24px"} color={iconBoxInside} />
              </IconBox>
            </Flex>
          </CardBody>
        </Card>
        <Card minH="83px">
          <CardBody>
            <Flex flexDirection="row" align="center" justify="center" w="100%">
              <Stat me="auto">
                <StatLabel
                  fontSize="sm"
                  color="gray.400"
                  fontWeight="bold"
                  pb=".1rem"
                >
                  Stopped Apps
                </StatLabel>
                <Flex>
                  {app.stop === "Please Wait" ? (
                    <StatNumber fontSize="sm" color={textColor}>
                      Please Wait
                    </StatNumber>
                  ) : (
                    <StatNumber fontSize="lg" color={textColor}>
                      {app.stop}
                    </StatNumber>
                  )}
                </Flex>
              </Stat>
              <IconBox as="box" h={"45px"} w={"45px"} bg={iconTeal}>
                <ClockIcon h={"24px"} w={"24px"} color={iconBoxInside} />
              </IconBox>
            </Flex>
          </CardBody>
        </Card>
      </SimpleGrid>
      <Grid
        templateColumns={{ sm: "1fr", lg: "1fr" }}
        templateRows={{ sm: "1fr", lg: "1fr" }}
        gap="24px"
        mb={{ lg: "26px" }}
        mt={{ lg: "26px" }}
      >
        <Card p="28px 10px 16px 0px" mb={{ sm: "26px", lg: "0px" }}>
          <CardHeader mb="20px" pl="22px">
            <Flex direction="column" alignSelf="flex-start">
              <Text fontSize="lg" color={textColor} fontWeight="bold" mb="6px">
                Applications Overview
              </Text>
              <Text fontSize="md" fontWeight="medium" color="gray.400">
                <Text as="span" color="green.400" fontWeight="bold">
                  {days[new Date().getDay()]}
                </Text>{" "}
                {monthNames[new Date().getMonth()]} {new Date().getDate()},{" "}
                {new Date().getFullYear()}
              </Text>
            </Flex>
          </CardHeader>
          <Box w="100%" h={{ sm: "300px" }} ps="8px">
            <LineChart app={app} />
          </Box>
        </Card>
        <Card p="16px">
          <CardBody>
            <Flex direction="column" w="100%">
              <BarChart />
              <Flex
                direction="column"
                mt="24px"
                mb="36px"
                alignSelf="flex-start"
              >
                <Text
                  fontSize="lg"
                  color={textColor}
                  fontWeight="bold"
                  mb="6px"
                >
                  Application Details
                </Text>
                <Text fontSize="md" fontWeight="medium" color="gray.400">
                  <Text as="span" color="green.400" fontWeight="bold">
                    Healthiness
                  </Text>{" "}
                  Level
                </Text>
              </Flex>
              <SimpleGrid gap={{ sm: "12px" }} columns={3}>
                {data.data.map((item, index) => {
                  return (
                    <Flex direction="column">
                      <Flex alignItems="center">
                        <Text
                          fontSize="sm"
                          color="gray.400"
                          fontWeight="semibold"
                        >
                          {WorkerName(item)}
                        </Text>
                      </Flex>
                      <Text
                        fontSize="lg"
                        color={textColor}
                        fontWeight="bold"
                        mb="6px"
                        my="6px"
                      >
                        {(
                          100 -
                          (data.logcount[index] * 100) / data.total
                        ).toFixed(2)}
                        %
                      </Text>
                      <Progress
                        colorScheme="teal"
                        borderRadius="12px"
                        h="5px"
                        value={100 - (data.logcount[index] * 100) / data.total}
                      />
                    </Flex>
                  );
                })}
              </SimpleGrid>
            </Flex>
          </CardBody>
        </Card>
      </Grid>
    </Flex>
  );
}
