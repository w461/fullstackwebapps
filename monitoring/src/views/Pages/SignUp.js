// Chakra imports
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Link,
  Switch,
  Text,
  Textarea,
  useColorModeValue,
  FormErrorMessage,
} from "@chakra-ui/react";
// Assets
import BgSignUp from "assets/img/BgSignUp.png";
import React, { useState, useEffect } from "react";
import * as axios from "axios";
import { Backendurl } from "config";

function SignUp() {
  const titleColor = useColorModeValue("teal.300", "teal.200");
  const textColor = useColorModeValue("gray.700", "white");
  const bgColor = useColorModeValue("white", "gray.700");
  const bgIcons = useColorModeValue("teal.200", "rgba(255, 255, 255, 0.5)");
  const [formData, setformData] = useState({
    email: "",
    name: "",
    profile: "",
    phone: "",
    location: "",
    password: "",
  });
  useEffect(() => {
    localStorage.clear();
  }, []);
  const [formError, setFormError] = useState({
    email: false,
    name: false,
    profile: false,
    phone: false,
    location: false,
    password: false,
  });

  const [emailnotValid, setemailnotValid] = useState(true);

  return (
    <Flex
      direction="column"
      alignSelf="center"
      justifySelf="center"
      overflow="hidden"
    >
      <Box
        position="absolute"
        minH={{ base: "70vh", md: "50vh" }}
        w={{ md: "calc(100vw - 50px)" }}
        borderRadius={{ md: "15px" }}
        left="0"
        right="0"
        bgRepeat="no-repeat"
        overflow="hidden"
        zIndex="-1"
        top="0"
        bgImage={BgSignUp}
        bgSize="cover"
        mx={{ md: "auto" }}
        mt={{ md: "14px" }}
      ></Box>
      <Flex
        direction="column"
        textAlign="center"
        justifyContent="center"
        align="center"
        mt="6.5rem"
        mb="30px"
      >
        <Text fontSize="4xl" color="white" fontWeight="bold">
          Welcome!
        </Text>
        <Text
          fontSize="md"
          color="white"
          fontWeight="normal"
          mt="10px"
          mb="26px"
          w={{ base: "90%", sm: "60%", lg: "40%", xl: "30%" }}
        >
          Create your account to start using this app.
        </Text>
      </Flex>
      <Flex alignItems="center" justifyContent="center" mb="60px" mt="20px">
        <Flex
          direction="column"
          w="445px"
          background="transparent"
          borderRadius="15px"
          p="40px"
          mx={{ base: "60px" }}
          bg={bgColor}
          boxShadow="0 20px 27px 0 rgb(0 0 0 / 5%)"
        >
          <Text
            fontSize="xl"
            color={textColor}
            fontWeight="bold"
            textAlign="center"
            mb="22px"
          >
            Register
          </Text>
          <ErrorForm
            data={formData.name}
            setData={(a) => {
              setformData({ ...formData, name: a });
            }}
            error={formError.name}
            setError={(a) => setFormError({ ...formError, name: a })}
            formName="Name"
            placeHolder="Your full name"
          />
          <ErrorForm
            data={formData.email}
            setData={(a) => {
              if (
                /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(a) &&
                a.length > 0
              ) {
                setemailnotValid(false);
              }
              setformData({ ...formData, email: a });
            }}
            error={formError.email}
            setError={(a) => setFormError({ ...formError, email: a })}
            formName="Email"
            placeHolder="Your email adresss"
            email={emailnotValid}
          />
          <ErrorForm
            data={formData.password}
            setData={(a) => {
              setformData({ ...formData, password: a });
            }}
            error={formError.password}
            setError={(a) => setFormError({ ...formError, password: a })}
            formName="Password"
            placeHolder="Your password"
            type="password"
          />{" "}
          <ErrorForm
            data={formData.phone}
            setData={(a) => {
              setformData({ ...formData, phone: a });
            }}
            error={formError.phone}
            setError={(a) => setFormError({ ...formError, phone: a })}
            formName="Phone"
            placeHolder="Your phone number"
          />
          <ErrorForm
            data={formData.location}
            setData={(a) => {
              setformData({ ...formData, location: a });
            }}
            error={formError.location}
            setError={(a) => setFormError({ ...formError, location: a })}
            formName="Location"
            placeHolder="Your location"
          />
          <ErrorForm
            data={formData.profile}
            setData={(a) => {
              setformData({ ...formData, profile: a });
            }}
            error={formError.profile}
            setError={(a) => setFormError({ ...formError, profile: a })}
            formName="Profile Info"
            placeHolder="Tell us about you...."
            type="textarea"
          />
          <FormControl>
            <FormControl display="flex" alignItems="center" mb="24px">
              <Switch id="remember-login" colorScheme="teal" me="10px" />
              <FormLabel htmlFor="remember-login" mb="0" fontWeight="normal">
                Remember me
              </FormLabel>
            </FormControl>
            <Button
              type="submit"
              bg="teal.300"
              fontSize="10px"
              color="white"
              fontWeight="bold"
              w="100%"
              h="45"
              mb="24px"
              _hover={{
                bg: "teal.200",
              }}
              _active={{
                bg: "teal.400",
              }}
              onClick={(e) => {
                e.preventDefault();
                if (
                  formData.name &&
                  formData.email &&
                  formData.password &&
                  formData.phone &&
                  formData.location &&
                  formData.profile
                ) {
                  axios({
                    method: "post",
                    url: Backendurl + "/api/auth/register",
                    headers: {},
                    data: formData,
                  })
                    .then(function (response) {
                      if (response.status === 200) {
                        window.location.replace("/#/auth/signin");
                      } else {
                        setformData({
                          email: "",
                          name: "",
                          profile: "",
                          phone: "",
                          location: "",
                          password: "",
                        });
                      }
                    })
                    .catch(function (error) {
                      setformData({
                        email: "",
                        name: "",
                        profile: "",
                        phone: "",
                        location: "",
                        password: "",
                      });
                    });
                } else {
                  setFormError({
                    email: formData.email === "" || emailnotValid,
                    name: formData.name === "",
                    profile: formData.profile === "",
                    phone: formData.phone === "",
                    location: formData.location === "",
                    password: formData.password === "",
                  });
                }
              }}
            >
              SIGN UP
            </Button>
          </FormControl>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            maxW="100%"
            mt="0px"
          >
            <Text color={textColor} fontWeight="medium">
              Already have an account?
              <Link
                color={titleColor}
                as="span"
                ms="5px"
                href="#"
                fontWeight="bold"
                onClick={(e) => {
                  e.preventDefault();
                  window.location.replace("/#/auth/signin");
                }}
              >
                Sign In
              </Link>
            </Text>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
}

export default SignUp;

function ErrorForm(props) {
  const handleInputChange = (e) => {
    props.setData(e.target.value);
    props.setError(false);
  };

  return (
    <FormControl isInvalid={props.error}>
      <FormLabel ms="4px" fontSize="sm" fontWeight="normal">
        {props.formName}
      </FormLabel>
      {props.type === "textarea" ? (
        <Textarea
          fontSize="sm"
          ms="4px"
          borderRadius="15px"
          type={props.type || "text"}
          placeholder={props.placeHolder}
          mb={!props.error ? "24px" : "0px"}
          size="lg"
          value={props.data}
          onChange={handleInputChange}
        />
      ) : (
        <Input
          fontSize="sm"
          ms="4px"
          borderRadius="15px"
          type={props.type || "text"}
          placeholder={props.placeHolder}
          mb={!props.error ? "24px" : "0px"}
          size="lg"
          value={props.data}
          onChange={handleInputChange}
        />
      )}
      {
        props.error ? (
          props.email || props.data.length > 1 ? (
            <FormErrorMessage mb="5px">
              {props.formName} not Valid
            </FormErrorMessage>
          ) : (
            <FormErrorMessage mb="5px">
              {props.formName} is required
            </FormErrorMessage>
          )
        ) : null
        // (
        // props.formName && props.email ? (
        //   <FormErrorMessage mb="5px">{props.formName} not Valid</FormErrorMessage>
        // ) : (
        //   <FormErrorMessage mb="5px">
        //     {props.formName} is required
        //   </FormErrorMessage>
        // )
      }
    </FormControl>
  );
}
