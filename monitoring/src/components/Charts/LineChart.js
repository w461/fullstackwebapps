import Axios from "axios";
import { Backendurl } from "config";
import React, { useEffect, useState } from "react";
import ReactApexChart from "react-apexcharts";

export default function LineChart(props) {
  const { app } = props;
  const [chartData, setchartData] = useState([]);
  const [chartOptions, setchartOptions] = useState({});

  useEffect(() => {
    Axios({
      method: "GET",
      url: Backendurl + "/api/history/lineData",
      headers: {},
      data: {},
    })
      .then((res) => {
        if (app.run !== "Please Wait" && app.stop !== "Please Wait") {
          let hour =
            Number(new Date().toLocaleString("id-ID", { hour: "numeric" })) + 1;
          let buf = [
            {
              name: "Running",
              data: res.data.run,
            },
            {
              name: "Stopped",
              data: res.data.stop,
            },
          ];
          buf[0].data[hour] = app.run;
          buf[1].data[hour] = app.stop;
          setchartData(buf);
        } else {
          setchartData([
            {
              name: "Running",
              data: res.data.run,
            },
            {
              name: "Stopped",
              data: res.data.stop,
            },
          ]);
        }
      })
      .catch((err) => {});

    setchartOptions({
      chart: {
        toolbar: {
          show: false,
        },
      },
      tooltip: {
        theme: "dark",
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: "smooth",
      },
      xaxis: {
        type: "datetime",
        categories: [
          "00",
          "01",
          "02",
          "03",
          "04",
          "05",
          "06",
          "07",
          "08",
          "09",
          "10",
          "11",
          "12",
          "13",
          "14",
          "15",
          "16",
          "17",
          "18",
          "19",
          "20",
          "21",
          "22",
          "23",
        ],
        labels: {
          style: {
            colors: "#c8cfca",
            fontSize: "12px",
          },
        },
      },
      yaxis: {
        labels: {
          style: {
            colors: "#c8cfca",
            fontSize: "12px",
          },
        },
      },
      legend: {
        show: false,
      },
      grid: {
        strokeDashArray: 5,
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "light",
          type: "vertical",
          shadeIntensity: 0.5,
          gradientToColors: undefined, // optional, if not defined - uses the shades of same color in series
          inverseColors: true,
          opacityFrom: 0.8,
          opacityTo: 0,
          stops: [],
        },
        colors: ["#4FD1C5", "#2D3748"],
      },
      colors: ["#4FD1C5", "#2D3748"],
    });
  }, [app.run, app.stop]);

  return (
    <ReactApexChart
      options={chartOptions}
      series={chartData}
      type="area"
      width="100%"
      height="100%"
    />
  );
}
