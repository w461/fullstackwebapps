import React, { useEffect, useState } from "react";
import Card from "components/Card/Card";
import Chart from "react-apexcharts";
import Axios from "axios";
import { Backendurl } from "config";
import { WorkerName } from "helper/worketToName";

export default function BarChart() {
  const [chartData, setchartData] = useState([]);
  const [chartOption, setchartOption] = useState({});

  useEffect(() => {
    Axios({
      method: "GET",
      url: Backendurl + "/api/logs/find",
      headers: {},
      data: {},
    })
      .then((res) => {
        let nameApps = [];
        let logApps = [];
        res.data.map((item) => {
          nameApps.push(WorkerName(item.name));
          logApps.push(item.count);
        });
        setchartData([
          {
            name: "Error/Warning Logs",
            data: logApps,
          },
        ]);
        setchartOption({
          chart: {
            toolbar: {
              show: false,
            },
          },
          tooltip: {
            style: {
              backgroundColor: "red",
              fontSize: "12px",
              fontFamily: undefined,
            },
            onDatasetHover: {
              style: {
                backgroundColor: "red",
                fontSize: "12px",
                fontFamily: undefined,
              },
            },
            theme: "dark",
          },
          xaxis: {
            categories: nameApps,
            show: false,
            labels: {
              show: false,
              style: {
                colors: "#fff",
                fontSize: "12px",
              },
            },
            axisBorder: {
              show: false,
            },
            axisTicks: {
              show: false,
            },
          },
          yaxis: {
            show: true,
            color: "#fff",
            labels: {
              show: true,
              style: {
                colors: "#fff",
                fontSize: "14px",
              },
            },
          },
          grid: {
            show: false,
          },
          fill: {
            colors: "#fff",
          },
          dataLabels: {
            enabled: false,
          },
          plotOptions: {
            bar: {
              borderRadius: 8,
              columnWidth: "12px",
            },
          },
          responsive: [
            {
              breakpoint: 768,
              options: {
                plotOptions: {
                  bar: {
                    borderRadius: 0,
                  },
                },
              },
            },
          ],
        });
      })

      .catch();
  }, []);
  return (
    <Card
      py="1rem"
      height={{ sm: "200px" }}
      width="100%"
      bg="linear-gradient(81.62deg, #313860 2.25%, #151928 79.87%)"
      position="relative"
    >
      <Chart
        options={chartOption}
        series={chartData}
        type="bar"
        width="100%"
        height="100%"
      />
    </Card>
  );
}
