import {
  Avatar,
  Badge,
  Button,
  Flex,
  Td,
  Text,
  Tr,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useState } from "react";
import Axios from "axios";
import { ChevronDownIcon } from "@chakra-ui/icons";
import { APIControlServer, UserBoleh } from "config";
import { WorkerName } from "helper/worketToName";

function TablesTableRow(props) {
  const { name, subdomain, domain, status, date, triger, settriger } = props;
  const textColor = useColorModeValue("gray.700", "white");
  const bgStatus = useColorModeValue("gray.400", "#1a202c");
  const colorStatus = useColorModeValue("white", "gray.400");

  const [statuser, setstatuser] = useState("idle");

  return (
    <Tr>
      <Td minWidth={{ sm: "250px" }} pl="0px">
        <Flex align="center" py=".8rem" minWidth="100%" flexWrap="nowrap">
          <Avatar
            src={`https://avatars.dicebear.com/api/bottts/${WorkerName(name)
              .toLowerCase()
              .replace(/\s/g, "")}.svg`}
            w="50px"
            me="18px"
            bg="transparant"
          />
          <Flex direction="column">
            <Text
              fontSize="md"
              color={textColor}
              fontWeight="bold"
              minWidth="100%"
            >
              {WorkerName(name)}
            </Text>
          </Flex>
        </Flex>
      </Td>

      <Td>
        <Flex direction="column">
          <Text fontSize="md" color={textColor} fontWeight="bold">
            {domain}
          </Text>
          <Text fontSize="sm" color="gray.400" fontWeight="normal">
            {subdomain}
          </Text>
        </Flex>
      </Td>
      <Td textAlign="center">
        <Badge
          bg={status === "Online" ? "green.400" : bgStatus}
          color={status === "Online" ? "white" : colorStatus}
          fontSize="16px"
          p="3px 10px"
          borderRadius="8px"
        >
          {status}
        </Badge>
      </Td>
      <Td>
        <Text
          fontSize="md"
          color={textColor}
          fontWeight="bold"
          pb=".5rem"
          style={{ textAlign: "center" }}
        >
          {date}
        </Text>
      </Td>
      <Td>
        <Text
          fontSize="md"
          color={textColor}
          fontWeight="bold"
          pb=".5rem"
          style={{ textAlign: "center" }}
        >
          {statuser}
        </Text>
      </Td>
      <Td>
        <Menu>
          <MenuButton
            fontSize="md"
            color="green.600"
            fontWeight="bold"
            cursor="pointer"
            disabled={!(UserBoleh.indexOf(localStorage.getItem("token")) > -1)}
            as={Button}
            rightIcon={<ChevronDownIcon />}
          >
            Actions
          </MenuButton>
          <MenuList>
            <MenuItem
              onClick={() => {
                setstatuser("restarting");
                Axios({
                  method: "post",
                  url: APIControlServer, //"url basta",
                  headers: {
                    "Bypass-Tunnel-Reminder": "true",
                    "Content-Type": "application/json",
                  },
                  data: {
                    container: name,
                    state: "restart",
                  },
                })
                  .then(function (response) {
                    setstatuser("restarted");
                    settriger(!triger);
                  })
                  .catch(function (error) {
                    setstatuser("error");
                  });
              }}
            >
              Restart
            </MenuItem>
            <MenuItem
              onClick={() => {
                setstatuser("starting");
                Axios({
                  method: "post",
                  url: APIControlServer, //"url basta",
                  headers: {
                    "Bypass-Tunnel-Reminder": "true",
                    "Content-Type": "application/json",
                  },
                  data: {
                    container: name,
                    state: "start",
                  },
                })
                  .then(function (response) {
                    setstatuser("started");
                    settriger(!triger);
                  })
                  .catch(function (error) {
                    setstatuser("error");
                  });
              }}
            >
              Start
            </MenuItem>
            <MenuItem
              onClick={() => {
                setstatuser("stopping");
                Axios({
                  method: "post",
                  url: APIControlServer, //"url basta",
                  headers: {
                    "Bypass-Tunnel-Reminder": "true",
                    "Content-Type": "application/json",
                  },
                  data: {
                    container: name,
                    state: "stop",
                  },
                })
                  .then(function (response) {
                    setstatuser("stopped");
                    settriger(!triger);
                  })
                  .catch(function (error) {
                    setstatuser("error");
                  });
              }}
            >
              Stop
            </MenuItem>
          </MenuList>
        </Menu>
      </Td>
    </Tr>
  );
}

export default TablesTableRow;
