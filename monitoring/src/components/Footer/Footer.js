/*eslint-disable*/
import React from "react";
import { Flex, Link, Text } from "@chakra-ui/react";

export default function Footer(props) {
  // const linkTeal = useColorModeValue("teal.400", "red.200");=
  return (
    <Flex
      flexDirection={{
        base: "column",
        xl: "row",
      }}
      alignItems={{
        base: "center",
        xl: "start",
      }}
      justifyContent="space-between"
      px="30px"
      pb="20px"
    >
      <Text
        color="gray.400"
        textAlign={{
          base: "center",
          xl: "start",
        }}
        mb={{ base: "20px", xl: "0px" }}
      >
        &copy; {1900 + new Date().getYear()},{" "}
        <Text as="span">
          {document.documentElement.dir === "rtl"
            ? " مصنوع من ❤️ بواسطة"
            : "Made with ❤️ by "}
        </Text>
        <Link
          // color={linkTeal}
          color="teal.400"
          // href=""
        >
          {document.documentElement.dir === "rtl"
            ? " توقيت الإبداعية"
            : "Wahyu Team "}
        </Link>
        & supported by
        <Link
          // color={linkTeal}
          color="teal.400"
          // href=""
        >
          {document.documentElement.dir === "rtl" ? "سيممبل " : " CHIP 8"}
        </Link>
        {document.documentElement.dir === "rtl"
          ? "للحصول على ويب أفضل"
          : " Infra Team"}
      </Text>
      <Text
        color="gray.400"
        textAlign={{
          base: "center",
          xl: "start",
        }}
        mb={{ base: "20px", xl: "0px" }}
      >
        <Link
          // color={linkTeal}
          color="teal.400"
          href="https://id.linkedin.com/in/vaghan-muhammad-sumadiredja-823155190"
        >
          Agan
        </Link>{" "}
        was here.
      </Text>
    </Flex>
  );
}
