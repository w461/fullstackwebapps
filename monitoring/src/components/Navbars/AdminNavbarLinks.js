// Chakra Imports
import { Button, Flex, Text, useColorModeValue } from "@chakra-ui/react";
// Custom Icons
import { ProfileIcon, SettingsIcon } from "components/Icons/Icons";
// Custom Components
import { SidebarResponsive } from "components/Sidebar/Sidebar";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import routes from "routes.js";
import * as axios from "axios";
import { Backendurl } from "config";

export default function HeaderLinks(props) {
  const { variant, children, fixed, secondary, onOpen, ...rest } = props;
  const [user, setuser] = useState({
    name: "",
  });

  useEffect(() => {
    axios({
      method: "get",
      url: Backendurl + "/api/user/find/" + localStorage.getItem("token"),
      headers: {},
      data: {},
    })
      .then((res) => {
        setuser({ name: res.data.data.name });
      })
      .catch((err) => {
        localStorage.clear();
        window.location.replace("/#/auth/signin");
      });
  }, []);

  // Chakra Color Mode
  let mainText = useColorModeValue("gray.700", "gray.200");
  let navbarIcon = useColorModeValue("gray.700", "gray.200");
  const [apils, setapils] = useState([]);
  useEffect(() => {
    let data = [];
    routes.map((rute) => {
      if (rute.category !== "account") {
        data.push(rute);
      }
      if(rute.category == "account"){
        let vw = []
        rute.views.map((viewrut)=>{
          if(viewrut.name !== "Hidden"){
            vw.push(viewrut)
          }
        })
        data.push({...rute, views:vw})
      }

    });
    setapils(data);
  }, []);

  if (secondary) {
    navbarIcon = "white";
    mainText = "white";
  }
  const settingsRef = React.useRef();
  return (
    <Flex
      pe={{ sm: "0px", md: "16px" }}
      w={{ sm: "100%", md: "auto" }}
      alignItems="center"
      flexDirection="row"
    >
      <NavLink to="/admin/profile">
        <Button
          ms="0px"
          px="0px"
          me={{ sm: "2px", md: "16px" }}
          color={navbarIcon}
          variant="transparent-with-icon"
          rightIcon={
            document.documentElement.dir ? (
              ""
            ) : (
              <ProfileIcon color={navbarIcon} w="22px" h="22px" me="0px" />
            )
          }
          leftIcon={
            document.documentElement.dir ? (
              <ProfileIcon color={navbarIcon} w="22px" h="22px" me="0px" />
            ) : (
              ""
            )
          }
        >
          <Text display={{ sm: "none", md: "flex" }}>{user.name}</Text>
        </Button>
      </NavLink>
      <SidebarResponsive
        logoText={props.logoText}
        secondary={props.secondary}
        routes={apils}
        {...rest}
      />
      <SettingsIcon
        cursor="pointer"
        ms={{ base: "16px", xl: "0px" }}
        me="16px"
        ref={settingsRef}
        onClick={props.onOpen}
        color={navbarIcon}
        w="18px"
        h="18px"
      />
    </Flex>
  );
}

HeaderLinks.propTypes = {
  variant: PropTypes.string,
  fixed: PropTypes.bool,
  secondary: PropTypes.bool,
  onOpen: PropTypes.func,
};
