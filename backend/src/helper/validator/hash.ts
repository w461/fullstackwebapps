import * as bcrypt from "bcrypt";

export function Hash_password(password: any): string {
  let hasil_enkripsi = bcrypt.hashSync(password, 10);
  return hasil_enkripsi;
}

export function Validator_password(hash: any, password_plain: any): boolean {
  let cek = bcrypt.compareSync(password_plain, hash);
  return cek;
}
