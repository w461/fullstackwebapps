import Axios from "axios";

export class HelperHit {
  private async hitAxios(ip: any) {
    return await Axios.get("http://" + ip, { timeout: 1000 * 3 })
      .then((response) => {
        let status = "Offline";
        if (response.status === 200) {
          status = "Online";
        }
        return status;
      })
      .catch((err) => {
        return "Offline";
      });
  }

  public async callCheck(query: any[]) {
    let appList: any = [];
    let promise: any[] = [];
    let i = 0;
    while (i < query.length) {
      promise.push(this.hitAxios(query[i].host));
      i++;
    }
    const status = await Promise.all(promise);
    query.map((val, idx) => {
      let buf = {
        name: val.name,
        status: status[idx],
        host: val.host.split(":")[0],
        port: val.host.split(":")[1],
      };
      appList.push(buf);
    });
    return appList;
  }
}
