import { decide } from "./reducerDecider";

export function reducer(log: object) {
  let temp: any = log;
  let num = Object.keys(log).length;
  if (num === 0) {
    return {
      msg: "Log kosong",
    };
  } else if (num === 1) {
    if (decide(temp)[0] === undefined) {
      return {
        msg: "log aneh",
      };
    }
    return decide(temp);
  } else {
    if (decide(temp).length === 0) {
      return {
        msg: "log aneh",
      };
    }
    return decide(temp);
  }
}

// console.log(reducer(log));
