export function datetimeConverter(date: string, time: string) {
  let datetime = new Date(
    date.split(".")[2] +
      "-" +
      date.split(".")[1] +
      "-" +
      date.split(".")[0] +
      "T" +
      time +
      "Z"
  );
  return datetime;
}
