import Axios from "axios";

export async function post(link: string, log: any) {
  return await Axios.post(link, log)
    .then(function (response) {
      return { res: response, stat: response.status };
    })
    .catch(function (error) {
      return { res: error, err: error };
    });
}
