/**
 * Nester function by hamdani (tanya aja kalo bingung)
 * @param data data json mentah yang mau di nest dengan sebuah key
 * @param idxAnak key parsing dari nest indexnya
 * @returns data yang sudah rapih dengan nesting yang baik :)
 */
export function Nester(data: any, idxAnak: any) {
  Object.keys(data).map(function (key, index) {
    if (key.includes(idxAnak)) {
      let split: any = key.split(idxAnak)[1].split("_")[1];
      if (data[idxAnak] == undefined) {
        data[idxAnak] = {};
      }
      data[idxAnak][split] = "";
      data[idxAnak][split] = data[key];
      delete data[key];
    }
  });
  return data;
}
