import { datetimeConverter } from "./datetimeConverter";

export function decide(temp: any) {
  let keys = Object.keys(temp);
  var reduced: any[] = [];
  var buf: any;
  for (let alert_type of keys) {
    // console.log(alert_type);
    if (temp[alert_type].type === "SEVERE") {
      buf = {
        type: "ERROR",
        service: temp[alert_type].frame,
        from: temp[alert_type].logSource,
        timestamp: datetimeConverter(
          temp[alert_type].date,
          temp[alert_type].time
        ),
      };
      reduced.push(buf);
    } else if (temp[alert_type].type === "CONFIG") {
      if (
        (temp[alert_type].text.errormsg =
          "Connection refused (Connection refused)")
      ) {
        buf = {
          type: "WARNING",
          service: temp[alert_type].message,
          from: temp[alert_type].logSource,
          timestamp: datetimeConverter(
            temp[alert_type].date,
            temp[alert_type].time
          ),
        };
      } else {
        buf = {
          type: temp[alert_type].type,
          service: temp[alert_type].service,
          from: temp[alert_type].logSource,
          timestamp: datetimeConverter(
            temp[alert_type].date,
            temp[alert_type].time
          ),
        };
      }
      reduced.push(buf);
    }
  }
  return reduced;
}
