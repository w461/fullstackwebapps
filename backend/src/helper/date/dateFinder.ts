export function TimeAdder(days: number) {
  let dateToday = new Date().getTime();
  let date = dateToday + days * 24 * 60 * 60 * 1000;
  return new Date(date).toLocaleString("id-ID", {
    day: "numeric",
    month: "numeric",
    year: "numeric",
  });
}
