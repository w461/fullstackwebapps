export function RouteFormater(px: string, rutearr: any[]): any[] {
  let rute: any[] = [];
  rutearr.map((val) => {
    val.route.map((value: any) => {
      value.route = `${px}${val.px}${value.route}`;
    });
    rute = [...rute, ...val.route];
  });
  return rute;
}
