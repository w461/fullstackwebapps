import { myDB } from "./connection/mainDB";
import express, { Request, Response } from "express";
import { Routes } from "./routes/routes";
import Schedule from "node-schedule";
import { CekJamDB, ClearCache } from "./job";

const app = express();
const cors = require("cors");

process.env.TZ = "Asia/Jakarta";

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

myDB.initDB();

const port = 12345;

Routes.forEach((route) => {
  (app as any)[route.method](
    route.route,
    (req: Request, res: Response, next: Function) => {
      const result = new (route.controller as any)()[route.action](
        req,
        res,
        next
      );
      if (result instanceof Promise) {
        result.then((result) =>
          result !== null && result !== undefined ? res.send(result) : undefined
        );
      } else if (result !== null && result !== undefined) {
        res.json(result);
      }
    }
  );
});

app.listen(port, () => {
  // scheduler insert here
  const job = Schedule.scheduleJob("0 * * * *", CekJamDB);
  const job2 = Schedule.scheduleJob("0 * * *", ClearCache);
  console.log(`Example app listening at http://3.210.30.177:${port}`);
});
