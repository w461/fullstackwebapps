import { TimeAdder } from "../helper/date/dateFinder";
import { HelperHit } from "../helper/hitapi";
import { AppsModel } from "../schema/apps";
import { HistoryModel } from "../schema/history";

export async function CekJamDB() {
  let query = await AppsModel.find().select(" -__v");
  let data = await new HelperHit().callCheck(query);
  let countOnline = 0;
  let countOffline = 0;
  for (let i = 0; i < data.length; i++) {
    if (data[i].status === "Offline") {
      countOffline = countOffline + 1;
    } else if (data[i].status === "Online") {
      countOnline = countOnline + 1;
    }
  }
  let countStatus = {
    run: countOnline,
    stop: countOffline,
  };

  let dateToday = TimeAdder(0);
  let hours = new Date().toLocaleString("id-ID", { hour: "numeric" });
  // cek jam ini sudah terdaftar di db history
  let hourDB = await HistoryModel.find({
    date: dateToday,
    hour: hours,
  });
  if (hourDB.length === 0) {
    // jika belum terdaftar, insert
    let history = new HistoryModel({
      date: dateToday,
      hour: hours,
      run: countStatus.run,
      stop: countStatus.stop,
    });
    await history.save();
  }
}

export async function ClearCache() {
  let dateTomorrow = TimeAdder(-1);
  // cek jam ini sudah terdaftar di db history
  await HistoryModel.deleteMany({
    date: dateTomorrow,
  });
}
