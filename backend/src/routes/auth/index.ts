import { AuthController } from "../../controller/auth";

export const AuthRoute = {
  px: "/auth",
  route: [
    {
      method: "post",
      route: "/register",
      controller: AuthController,
      action: "register",
    },
    {
      method: "post",
      route: "/login",
      controller: AuthController,
      action: "login",
    },
  ],
};
