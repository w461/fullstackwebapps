import { LogsController } from "../../controller/logs";

export const LogsRoute = {
  px: "/logs",
  route: [
    {
      method: "get",
      route: "/find/total",
      controller: LogsController,
      action: "findLogs",
    },
    {
      method: "get",
      route: "/find",
      controller: LogsController,
      action: "findLogsByApp",
    },
  ],
};
