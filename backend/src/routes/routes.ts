import { RouteFormater } from "../helper/route/Refactor";
import { AppsRoute } from "./apps";
import { AuthRoute } from "./auth";
import { UserRoute } from "./user";
import { LogsRoute } from "./logs";
import { HistoryRoute } from "./history";

export const Routes = RouteFormater("/api", [
  AuthRoute,
  UserRoute,
  AppsRoute,
  LogsRoute,
  HistoryRoute,
]);
