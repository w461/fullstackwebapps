import { AppsController } from "../../controller/apps";

export const AppsRoute = {
  px: "/apps",
  route: [
    {
      method: "post",
      route: "/create",
      controller: AppsController,
      action: "createApps",
    },
    {
      method: "post",
      route: "/delete/:id",
      controller: AppsController,
      action: "deleteApps",
    },
    {
      method: "post",
      route: "/listen",
      controller: AppsController,
      action: "listenApps",
    },
    {
      method: "get",
      route: "/find",
      controller: AppsController,
      action: "findApps",
    },
    {
      method: "get",
      route: "/check",
      controller: AppsController,
      action: "check",
    },
  ],
};
