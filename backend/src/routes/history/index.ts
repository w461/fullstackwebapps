import { HistorysController } from "../../controller/history";

export const HistoryRoute = {
  px: "/history",
  route: [
    {
      method: "get",
      route: "/lineData",
      controller: HistorysController,
      action: "lineData",
    },
  ],
};
