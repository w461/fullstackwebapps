import { UserController } from "../../controller/user";

export const UserRoute = {
  px: "/user",
  route: [
    {
      method: "get",
      route: "/find/:id",
      controller: UserController,
      action: "findbyid",
    },
  ],
};
