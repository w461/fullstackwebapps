import { Request, Response } from "express";
import { EmailVal } from "../helper/validator/email";
import { Validator_password } from "../helper/validator/hash";

export class MainController {
  output: any;
  status: number = 200;
  req: Request;
  res: Response;

  constructor(req: Request, res: Response) {
    this.req = req;
    this.res = res;
  }

  public checkEachIndexArrayNotUndefined(data: any): boolean {
    let check = true;
    data.forEach((element: any) => {
      if (element === undefined) {
        check = false;
        this.output = {
          msg: "Something is missing",
        };
        this.status = 406;
      }
    });
    return check;
  }

  public validateEmail(email: string): boolean {
    let check = true;
    if (!EmailVal(email)) {
      check = false;
      this.output = {
        msg: "Email is not valid",
      };
      this.status = 406;
    }
    return check;
  }

  public arrayDataIsEmpty(data: any): boolean {
    let check = true;
    if (data.length !== 0 && data[0] !== null) {
      check = false;
      this.output = {
        msg: "Data already Exist",
      };
      this.status = 400;
    }
    return check;
  }

  public queryExist(data: any, msg: string = "Data"): boolean {
    let check = true;
    if (!data) {
      check = false;
      this.output = {
        msg: `${msg} Not Found`,
      };
      this.status = 404;
    }
    return check;
  }

  public checkPassword(hash: any, password: string): boolean {
    let check = true;
    if (!Validator_password(hash, password)) {
      check = false;
      this.output = {
        msg: "Invalid Credentials",
      };
      this.status = 406;
    }
    return check;
  }

  public checkArrayObjectIncludeKey(data: any[], key: string[]): boolean {
    let check = true;
    data.forEach((element: any) => {
      key.forEach((key) => {
        if (!element[key]) {
          check = false;
          this.output = {
            msg: "Something is missing",
          };
          this.status = 406;
        }
      });
    });
    return check;
  }
}
