import { NextFunction, Request, Response } from "express";
import { MainController } from "../main";
import { Hash_password } from "../../helper/validator/hash";
import { UserModel } from "../../schema/user";

export class AuthController extends MainController {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  async login(request: Request, response: Response, next: NextFunction) {
    let data = request.body;
    if (this.checkEachIndexArrayNotUndefined([data.email, data.password])) {
      let userModel = UserModel;
      let user = await userModel.findOne({ email: data.email });

      if (this.queryExist(user, "User")) {
        if (this.checkPassword(user?.password, data.password)) {
          this.output = {
            msg: "Login Success ",
            data: user?.id,
          };
        }
      }
    }
    response.status(this.status);
    return this.output;
  }

  async register(request: Request, response: Response, next: NextFunction) {
    let data = request.body;
    if (
      this.checkEachIndexArrayNotUndefined([
        data.name,
        data.profile,
        data.email,
        data.phone,
        data.location,
        data.password,
      ])
    ) {
      if (this.validateEmail(data.email)) {
        let user = UserModel;
        let somedata = await user.find({
          $or: [{ email: data.email }, { phone: data.phone }],
        });
        if (this.arrayDataIsEmpty(somedata)) {
          data.password = Hash_password(data.password);
          let newUser = new user(data);
          await newUser.save();
          this.output = {
            msg: "Data inserted successfully",
          };
        }
      }
    }
    response.status(this.status);
    return this.output;
  }
}
