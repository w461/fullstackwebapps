import { NextFunction, Request, Response } from "express";
import { MainController } from "../main";
import { UserModel } from "../../schema/user";

export class UserController extends MainController {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  async findbyid(request: Request, response: Response, next: NextFunction) {
    let id = request.params.id;

    try {
      let user = await UserModel.findById(id);
      if (this.queryExist(user, "User")) {
        let userdata = user;
        userdata.password = undefined;
        userdata.__v = undefined;
        userdata._id = undefined;

        this.output = {
          msg: "User found",
          data: userdata,
        };
      }
    } catch (error) {
      this.status = 404;
      this.output = {
        msg: "User not found",
      };
    }

    response.status(this.status);
    return this.output;
  }
}
