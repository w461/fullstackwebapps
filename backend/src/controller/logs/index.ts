import { NextFunction, Request, Response } from "express";
import { MainController } from "../main";
import { LogsModel } from "../../schema/logs";
import { TimeAdder } from "../../helper/date/dateFinder";
import { AppsModel } from "../../schema/apps";

export class LogsController extends MainController {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  async findLogs(request: Request, response: Response, next: NextFunction) {
    let queryHariIni = await LogsModel.find({
      date: { $regex: TimeAdder(0) + ".*" },
    });
    let queryKemarin = await LogsModel.find();

    this.output = {
      dataLog: queryKemarin.length - queryHariIni.length,
      selisih: queryHariIni.length,
    };
    response.status(this.status);
    return this.output;
  }

  async findLogsByApp(
    request: Request,
    response: Response,
    next: NextFunction
  ) {
    let apps = await AppsModel.find();
    let arr: any[] = [];
    let promise: any[] = [];
    let i = 0;
    while (i < apps.length) {
      promise.push(await LogsModel.find({ idApps: apps[i]._id }));
      i++;
    }
    const data = await Promise.all(promise);
    data.map((val, idx) => {
      arr[idx] = { name: apps[idx].name, count: val.length };
    });
    this.output = arr;
    response.status(this.status);
    return this.output;
  }
}
