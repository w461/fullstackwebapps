import { NextFunction, Request, Response } from "express";
import { MainController } from "../main";
import { HistoryModel } from "../../schema/history";
import { TimeAdder } from "../../helper/date/dateFinder";

export class HistorysController extends MainController {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  async lineData(request: Request, response: Response, next: NextFunction) {
    let rawData = await HistoryModel.find({ date: TimeAdder(0) });
    // console.log(cek);
    let runArr = new Array(24).fill(0);
    let stopArr = new Array(24).fill(0);
    rawData.map((element) => {
      let hour = Number(element.hour);
      runArr[hour] = element.run;
      stopArr[hour] = element.stop;
    });
    this.output = {
      run: runArr,
      stop: stopArr,
    };
    response.status(this.status);
    return this.output;
  }
}
