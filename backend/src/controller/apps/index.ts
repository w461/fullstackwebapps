import { NextFunction, Request, Response } from "express";
import { MainController } from "../main";
import { AppsModel } from "../../schema/apps";
import { LogsModel } from "../../schema/logs";
import Axios from "axios";
import { ParserGrok } from "../../helper/parser";
import { reducer } from "../../helper/reducer";
import { post } from "../../helper/post_send";
import { ToTitleCase } from "../../helper/titlecase";
import { APIBasta } from "../../config";

export class AppsController extends MainController {
  constructor(req: Request, res: Response) {
    super(req, res);
  }

  async createApps(request: Request, response: Response, next: NextFunction) {
    let data = request.body;
    if (
      this.checkEachIndexArrayNotUndefined([data.name, data.host, data.type])
    ) {
      let appsada = await AppsModel.find({
        host: data.host,
      });
      let newApps: any;
      if (this.arrayDataIsEmpty(appsada)) {
        newApps = new AppsModel({ ...data });
        await newApps.save();
      }

      let log = new LogsModel({
        tipe: data.type,
        date: new Date().toLocaleString("id-ID").replace(/, /, " "),
        idApps: appsada.length > 0 ? appsada[0]._id : newApps._id,
      });
      await log.save();
      this.status = 200;
      this.output = {
        msg: "Data inserted successfully",
      };
    }
    response.status(this.status);
    return this.output;
  }

  private async hitAxios(ip: any) {
    return await Axios.get("http://" + ip, { timeout: 1000 * 3 })
      .then((response) => {
        let status = "Offline";
        if (response.status === 200) {
          status = "Online";
        }
        return status;
      })
      .catch((err) => {
        return "Offline";
      });
  }

  private async callCheck(query: any[]) {
    let appList: any = [];
    let promise: any[] = [];
    let i = 0;
    while (i < query.length) {
      promise.push(this.hitAxios(query[i].host));
      i++;
    }
    const status = await Promise.all(promise);
    query.map((val, idx) => {
      let buf = {
        name: val.name,
        status: status[idx],
        host: val.host.split(":")[0],
        port: val.host.split(":")[1],
      };
      appList.push(buf);
    });
    return appList;
  }

  async findApps(request: Request, response: Response, next: NextFunction) {
    let query = await AppsModel.find().select(" -__v");
    this.output = await this.callCheck(query);
    response.status(this.status);
    return this.output;
  }

  async check(request: Request, response: Response, next: NextFunction) {
    let query = await AppsModel.find().select(" -__v");
    let data = await this.callCheck(query);
    let countOnline = 0;
    let countOffline = 0;
    for (let i = 0; i < data.length; i++) {
      if (data[i].status === "Offline") {
        countOffline = countOffline + 1;
      } else if (data[i].status === "Online") {
        countOnline = countOnline + 1;
      }
    }
    let countStatus = {
      run: countOnline,
      stop: countOffline,
    };
    return countStatus;
  }

  async deleteApps(request: Request, response: Response, next: NextFunction) {
    let id = request.params.id;
    try {
      let query = await AppsModel.findById(id);
      await LogsModel.deleteMany({ idApps: id });
      await query.remove();
      this.output = {
        msg: "success",
      };
    } catch (error) {
      this.output = {
        msg: "failed",
      };
    }
    response.status(this.status);
    return this.output;
  }

  async listenApps(request: Request, response: Response, next: NextFunction) {
    let body = request.body;

    if (this.checkEachIndexArrayNotUndefined([body.ip, body.log])) {
      try {
        let data = ParserGrok(body.log); //note dari Faisa: Tambahin sourceLog di JSON hasil ParserGrok
        let reduced: any = reducer(data);
        if (Array.isArray(reduced)) {
          reduced.map((value: any, idx: number) => {
            reduced[idx].service = value.service.split(" ")[0];
            reduced[idx].from = body.ip;
          });

          reduced.forEach(async (element) => {
            post(APIBasta, element); //file server ada di repo parser-to-automator
            post("https://wahyuboard.my.id/api/apps/create", {
              name: element.service,
              host: element.from,
              type: ToTitleCase(element.type),
            });
          });
        }
        this.output = {
          msg: "success",
        };
      } catch (error) {
        this.output = {
          msg: "error",
        };
        this.status = 400;
      }
    }

    response.status(this.status);
    return this.output;
  }
}
