export const Pattern = {
  config: `%{NOTSPACE:type} \\[%{URIHOST:service}\\] %{TIME:time} %{DATE:date} @ %{URIHOST:url}\\$%{WORD:tag}
  \\[Message\\] %{GREEDYDATA:message}
  \\[Source\\] %{URIHOST:source_url}\\$%{WORD:source_tag}\\@%{WORD:source_id}
  \\[Thread\\] %{NOTSPACE:thread} \\(%{INT:thread_number}\\)
  \\[Exception\\] %{GREEDYDATA:exception}
    \\[Text\\] %{GREEDYDATA:text_errortype}: %{GREEDYDATA:text_errorwhy}: %{GREEDYDATA:text_errormsg}
    \\[Cause\\] %{GREEDYDATA:cause}
      \\[Text\\] %{GREEDYDATA:cause_text}`,

  fine: `%{NOTSPACE:type} \\[%{URIHOST:service}\\] %{TIME:time} %{DATE:date} @ %{NOTSPACE:module}
  \\[Message\\] frame received: %{GREEDYDATA:frame}
  \\[Source\\] \\[Transport:%{NOTSPACE:source_transport};To:/%{GREEDYDATA:source_ipto};From:/%{GREEDYDATA:source_ipfrom};KeepAlive:%{GREEDYDATA:source_keepalive};Framer:%{GREEDYDATA:source_framer} \\<###\\>\\]
  \\[Thread\\] %{NOTSPACE:thread} \\(%{INT:thread_number}\\)`,

  info: `%{NOTSPACE:type} \\[%{URIHOST:service}\\] %{TIME:time} %{DATE:date} @ %{NOTSPACE:module}
  \\[Message\\] message received: %{GREEDYDATA:message}
  \\[Source\\] \\[Protocol:%{NOTSPACE:protocol} \\(%{GREEDYDATA:version}\\);Client:\\[Transport:%{GREEDYDATA:client_transport};To:%{GREEDYDATA:client_to};From:%{GREEDYDATA:client_from};KeepAlive:%{GREEDYDATA:client_keepAlive};Framer:%{GREEDYDATA:client_framer}\\]\\]`,

  severe: `%{NOTSPACE:type} \\[%{URIHOST:service}\\] %{TIME:time} %{DATE:date} @ %{NOTSPACE:module}
  \\[Message\\] %{GREEDYDATA:frame}
  \\[Source\\] Instance:\\[UnitID:%{INT:unitid};External:{Host:\\[Protocol:%{GREEDYDATA:protocol};Config:%{GREEDYDATA:config}\\]}\\]
  \\[Thread\\] %{NOTSPACE:thread} \\(%{INT:thread_number}\\)
  \\[Exception\\] %{GREEDYDATA:exception}`,
};

export const Childs = {
  config: ["source", "text"],
  fine: ["source"],
  info: ["client"],
};
