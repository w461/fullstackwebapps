import { Model, Schema, Document, model } from "mongoose";

export type UserType = (UserSchema & Document) | any;

export interface UserSchema {
  name: {
    type: String;
    required: true;
  };
  profile: {
    type: String;
    required: true;
  };
  email: {
    type: String;
    required: true;
  };
  phone: {
    type: String;
    required: true;
  };
  location: {
    type: String;
    required: true;
  };
  password: {
    type: String;
    required: true;
  };
}

const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  profile: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  location: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

export const UserModel: Model<UserType> = model<UserType>(
  "User",
  UserSchema,
  "user"
);
