import { Model, Schema, Document, model } from "mongoose";

type LogsType = (LogsSchema & Document) | any;

export interface LogsSchema {
  tipe: {
    type: String;
    required: true;
  };
  idApps: {
    type: String;
    required: true;
  };
  date: {
    type: String;
    required: true;
  };
}

const LogsSchema = new Schema({
  tipe: {
    type: String,
    required: true,
  },
  idApps: {
    type: Schema.Types.ObjectId,
    ref: "Apps",
  },
  date: {
    type: String,
    required: true,
  },
});

export const LogsModel: Model<LogsType> = model<LogsType>(
  "Logs",
  LogsSchema,
  "logs"
);
