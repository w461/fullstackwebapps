import { Model, Schema, Document, model } from "mongoose";

type AppsType = (AppsSchema & Document) | any;

export interface AppsSchema {
  name: {
    type: String;
    required: true;
  };
  host: {
    type: String;
    required: true;
  };
}

const AppsSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  host: {
    type: String,
    required: true,
  },
});

export const AppsModel: Model<AppsType> = model<AppsType>(
  "Apps",
  AppsSchema,
  "apps"
);
