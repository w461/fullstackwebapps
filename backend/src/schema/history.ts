import { Model, Schema, Document, model } from "mongoose";

export type HistoryType = (HistorySchema & Document) | any;

export interface HistorySchema {
  date: {
    type: String;
    required: true;
  };
  hour: {
    type: String;
    required: true;
  };
  run: {
    type: Number;
    required: true;
  };
  stop: {
    type: Number;
    required: true;
  };
}

const HistorySchema = new Schema({
  date: {
    type: String,
    required: true,
  },
  hour: {
    type: String,
    required: true,
  },
  run: {
    type: Number,
    required: true,
  },
  stop: {
    type: Number,
    required: true,
  },
});

export const HistoryModel: Model<HistoryType> = model<HistoryType>(
  "History",
  HistorySchema,
  "history"
);
